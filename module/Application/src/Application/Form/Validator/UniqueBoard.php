<?php
namespace Application\Form\Validator;

use Doctrine\ODM\MongoDB\DocumentManager;
use Zend\Validator\AbstractValidator;

class UniqueBoard extends AbstractValidator
{
    const BOARD_EXISTS = 'boardExists';

    protected $messageTemplates = [
        self::BOARD_EXISTS =>  "Board '%value%' already exists"
    ];

    protected $dm;
    
    public function isValid($value, $context = null)
    {
        $this->setValue($value);

        $board = $this->dm->getRepository('Application\Document\Board')->findOneBy(['name' => $value]);

        if (null === $board || $board->getId() == $context['id']) {
            return true;
        }

        $this->error(self::BOARD_EXISTS);
        return false;
    }

    public function setDocumentManager(DocumentManager $documentManager)
    {
        $this->dm = $documentManager;
    }
}