<?php
namespace Application\Form;

use Application\Form\Validator\UniqueBoard;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Validator\Regex;

class Board extends Form implements InputFilterProviderInterface, ServiceLocatorAwareInterface
{
    public function init()
    {
        $this->add([
            'type' => 'text',
            'name' => 'name',
            'required' => true,
            'attributes' => [
                'id' => 'name',
                'placeholder' => 'Enter a board name'
            ]
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'id',
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'submit',
            'attributes' => [
                'type' => 'submit',
            ],
            'options' => [
                'label' => 'Save'
            ]
        ]);
    }

    public function getInputFilterSpecification()
    {
        $serviceLocator = $this->getServiceLocator();

        $documentManager = $serviceLocator->get('doctrine.documentmanager.odm_default');
        $nameValidator = new Regex('/^[a-zA-Z0-9][a-zA-Z0-9_-\s]*$/');

        $boardExistsValidator = new UniqueBoard();
        $boardExistsValidator->setDocumentManager($documentManager);
        
        return [
            'name' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    $nameValidator,
                    $boardExistsValidator
                ]
            ],
        ];
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        $formElementManager = $this->serviceLocator;
        return $formElementManager->getServiceLocator();
    }
}