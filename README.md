# Рекламная доска

## Установка
Следущие шаги предполагают, что у Вас уже установлены и настроены PHP, MongoDb, Apache2 или Nginx. Также необходимы менеджеры пакетов Composer и Bower.

> Вы можеть посмотреть пример, как настроить Apache2 веб сервер для ZF2 приложения [здесь](https://framework.zend.com/manual/2.4/en/user-guide/skeleton-application.html#using-the-apache-web-serve)

1. Скачайте и скопируйте файлы приложения в рабочую директорию веб сервера;
2. В консоле перейдите в папку с приложением и выполните команду `composer install`;
3. Затем перейдите в папку *public* приложения и выполните команду `bower install`;
3. Перейдите в корневую папку приложения и переименуйте файл `config/autoload/module.doctrine-mongo-odm.local.php.dist`  в  `config/autoload/module.doctrine-mongo-odm.local.php`;
4. Укажите настройки соединения с базой в файле `config/autoload/module.doctrine-mongo-odm.local.php`;
5. Запустите приложение в браузере

## Системные требования
1. PHP версии 5.6;
2. MongoDb версия 2.6 и выше;
3. Apache2 или Nginx веб сервер.
4. Менеджеры пакетов Composer и Bower