<?php
namespace Application\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 */
class Board
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\String
     * @ODM\UniqueIndex
     */
    protected $name;

    /**
     * @ODM\EmbedMany(targetDocument="Box")
     */
    private $boxes = [];

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getBoxes()
    {
        return $this->boxes;
    }

    public function setBoxes(array $boxes = [])
    {
        $this->boxes = $boxes;
    }
}