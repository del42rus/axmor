<?php
namespace Application\Controller;

use Application\Document\Board;
use Application\Document\Box;
use Doctrine\ODM\MongoDB\DocumentManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Stdlib\Glob;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

use Zend\Form\FormElementManager\FormElementManagerV2Polyfill as FormManager;

class IndexController extends AbstractActionController
{
    protected $dm;
    protected $formManager;

    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    public function setFormManager(FormManager $formManager)
    {
        $this->formManager = $formManager;
    }

    public function indexAction()
    {
        $boards = $this->dm->getRepository('Application\Document\Board')->findAll();

        $imageDir = getcwd() . '/public/images';
        $imageThumbsDir = $imageDir . '/thumbs';

        $files = Glob::glob($imageThumbsDir . '/*.jpg', 0);

        // Prepare our image library
        $images = [];
        foreach ($files as $file) {
            $filename = basename($file);
            list($width, $height) = getimagesize($imageDir . '/' . $filename);

            $images[] =  [
                'filename' => $filename,
                'width' => $width,
                'height' => $height,
            ];
        }

        $form = $this->formManager->get('Application\Form\Board');

        return new ViewModel([
            'form' => $form,
            'boards' => $boards,
            'images' => $images
        ]);
    }
    
    public function saveAction()
    {
        $request = $this->getRequest();
        
        if (!$request->isXmlHttpRequest() && !$request->isPost()) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $form = $this->formManager->get('Application\Form\Board');
        $boardName = $request->getPost('boardName');

        $data = [
            'id' => $request->getPost('id'),
            'name' => $boardName
        ];

        $form->setData($data);

        if ($form->isValid()) {

            if ($id = $request->getPost('id')) {
                $board = $this->dm->getRepository('Application\Document\Board')->find($id);
            } else {
                $board = new Board();
            }

            $board->setName($boardName);

            $newBoxes = [];
            $boxes = $request->getPost('boxes') ?: [];

            // Add boxes to the board
            foreach ($boxes as $element) {
                $box = new Box();
                $box->populate($element);

                $newBoxes[] = $box;
            }

            $board->setBoxes($newBoxes);

            // Save the board
            $this->dm->persist($board);
            $this->dm->flush();

            return new JsonModel([
                'success' => 1,
                'boardId' => $board->getId(),
                'boardName' => $board->getName()
            ]);
        }

        return new JsonModel([
            'success' => 0,
            'messages' => $form->getMessages()
        ]);
    }

    public function loadBoardAction()
    {
        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() && !$request->isPost()) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $board = $this->dm->getRepository('Application\Document\Board')->find($request->getPost('id'));
        $boxes = $board->getBoxes();

        foreach ($boxes as $box) {
            $result[] = $box->getArrayCopy();
        }
        
        return new JsonModel([
            'boardId' => $board->getId(),
            'boardName' => $board->getName(),
            'boxes' => $result
        ]);
    }
}