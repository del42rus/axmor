<?php
namespace Application;

return [
    'controllers' => [
        'factories' => [
            'Application\Controller\Index' => 'Application\Factory\IndexControllerFactory',
        ],
    ],

    'router' => [
        'routes' => [
            'home' => [
                'type'    => 'literal',
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => 'Application\Controller\Index',
                        'action' => 'index'
                    ],
                ],
            ],
            'default' => [
                'type'    => 'segment',
                'options' => [
                    'route'    => '/:action',
                    'constraints' => [
                        'action' => '[a-zA-Z0-9_-]+',
                    ],
                    'defaults' => [
                        'controller' => 'Application\Controller\Index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'wildcard' => [
                        'type' => 'wildcard',
                        'options' => [
                            'key_value_delimiter' => '/',
                            'param_delimiter' => '/',
                        ],
                        'may_terminate' => true,
                    ],
                ],
            ],
        ],
    ],

    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'layout/layout' =>  __DIR__ . '/../view/layout/index/layout.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],

    'doctrine' => [
        'configuration' => [
            'odm_default' => [
                'driver'             => 'odm_default',
                'generate_proxies'   => true,
                'proxy_dir'          => 'data/DoctrineMongoODMModule/Proxy',
                'proxy_namespace'    => 'DoctrineMongoODMModule\Proxy',
            ]
        ],

        'documentmanager' => [
            'odm_default' => [
                'connection'    => 'odm_default',
                'configuration' => 'odm_default',
                'eventmanager' => 'odm_default'
            ]
        ],

        'eventmanager' => [
            'odm_default' => [
                'subscribers' => []
            ]
        ],

        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver',
                'paths' => [__DIR__ . '/../src/' . __NAMESPACE__ . '/Document']
            ],
            'odm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Document' => __NAMESPACE__ . '_driver'
                ]
            ]
        ],
    ]
];
