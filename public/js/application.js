var Application = (function () {
    var $form = $('form');
    var $board = $('.board > .content');
    var $boardSelect = $('select[name="boardId"]');
    var $boardIdInput = $form.find('input[name="id"]');
    var $boardNameInput = $form.find('input[name="name"]');
    var $imageContainer = $('.image-container');
    var $zoomControl = $('.zoom-slider');

    var zoom = 1;

    var containmentW,
        containmentH,
        objW,
        objH,
        borderWidth;

    // this creates the selected variable
    // we are going to store the selected objects in here
    // See http://celery94.github.io/javascript/2016/01/21/jquery-ui-selectable-draggable.html
    var selected = $([]), offset = {top:0, left:0};

    function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

        var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

        return { width: srcWidth * ratio, height: srcHeight * ratio };
    }

    var dropImage = function ($element, $target) {
        var image = $element.attr('data-full-image');

        var imageWidth = $element.attr('data-image-width');
        var imageHeight = $element.attr('data-image-height');
        var maxWidth = $target.width();
        var maxHeight = $target.height();

        // Resize image proportionally
        var size = calculateAspectRatioFit(imageWidth, imageHeight, maxWidth, maxHeight);

        var $img = jQuery('<img>', {
            src: image,
            css: {
                width: size.width + 'px',
                height: size.height + 'px'
            },
            attr: {
                'data-width': imageWidth,
                'data-height': imageHeight
            }
        });

        $target.find('img').remove();
        $img.appendTo($target);
    };

    // Make box draggable, droppable, resizable
    $.fn.interactive = function(){
        this.each(function(){
            $(this).draggable({
                // See http://celery94.github.io/javascript/2016/01/21/jquery-ui-selectable-draggable.html
                // containment: $board,
                start: function (e, ui) {
                    if (!$(this).is(".ui-selected")) {
                        $(".ui-selected").removeClass("ui-selected");
                    }
                    selected = $(".ui-selected").each(function () {
                        var element = $(this);
                        element.data("offset", element.offset());
                    });
                    offset = $(this).offset();

                    ui.position.left = 0;
                    ui.position.top = 0;

                    containmentW = $board.width() * zoom;
                    containmentH = $board.height() * zoom;
                    objW = $(this).outerWidth() * zoom;
                    objH = $(this).outerHeight() * zoom;
                },
                drag: function (event, ui) {
                    var draggedTop = ui.position.top - offset.top, draggedLeft = ui.position.left - offset.left;
                    selected.not(this).each(function () {
                        var element = $(this), off = element.data("offset");
                        element.css({top: off.top + draggedTop, left: off.left + draggedLeft});
                    });

                    var changeLeft = ui.position.left - ui.originalPosition.left; // find change in left
                    var newLeft = ui.originalPosition.left + changeLeft / zoom; // adjust new left by our zoom

                    var changeTop = ui.position.top - ui.originalPosition.top; // find change in top
                    var newTop = ui.originalPosition.top + changeTop / zoom; // adjust new top by our zoom

                    // Constrain dragging inside the board
                    // right bound check
                    if(ui.position.left  > containmentW - objW) {
                        newLeft = (containmentW - objW) / zoom;
                    }

                    // left bound check
                    if(newLeft < 0) {
                        newLeft = 0;
                    }

                    // bottom bound check
                    if(ui.position.top  > containmentH - objH) {
                        newTop = (containmentH - objH) / zoom;
                    }

                    // top bound check
                    if(newTop < 0) {
                        newTop = 0;
                    }

                    // fix position
                    ui.position.left = newLeft;
                    ui.position.top = newTop;
                }
            })
            .resizable({
                start: function (e, ui) {
                    borderWidth = $(this).outerWidth() - $(this).width();
                    
                    containmentW = $board.width() - borderWidth;
                    containmentH = $board.height() - borderWidth;
                },
                resize: function (e, ui) {
                    var changeWidth = ui.size.width - ui.originalSize.width; // find change in width
                    var newWidth = ui.originalSize.width  + changeWidth / zoom; // adjust new width by our zoomScale

                    var changeHeight = ui.size.height - ui.originalSize.height; // find change in height
                    var newHeight = ui.originalSize.height  + changeHeight / zoom; // adjust new height by our zoomScale

                    // Constrain resizing
                    if (changeWidth) {
                        if (ui.position.left + newWidth < containmentW - borderWidth) {
                            ui.size.width = newWidth;
                        } else {
                            ui.size.width = containmentW - ui.position.left
                        }
                    }

                    if (changeHeight) {
                        if (ui.position.top + newHeight < containmentH) {
                            ui.size.height = newHeight;
                        } else {
                            ui.size.height = containmentH - ui.position.top
                        }
                    }

                    var $img = $(this).find('img');

                    if ($img.length) {
                        var imageWidth = $img.attr('data-width');
                        var imageHeight = $img.attr('data-height');

                        var size = calculateAspectRatioFit(imageWidth, imageHeight, ui.size.width , ui.size.height);

                        $img.css({width: size.width + 'px', height: size.height + 'px'})
                    }
                }
            })
            .droppable({
                accept: ".image-container > img",
                classes: {
                    "ui-droppable-active": "ui-state-highlight"
                },
                drop: function( event, ui ) {
                    dropImage(ui.draggable, $(this));
                }
            }).uitooltip({
                position: {my: 'center bottom' , at: 'center top-10'},
                items: ".box",
            }).dblclick(function () {

                var width = $(this).outerWidth();
                var height = $(this).outerHeight();
                var positionLeft = Math.round($(this).position().left / zoom);
                var positionTop = Math.round($(this).position().top / zoom);

                var content = 'w: ' + width + 'px; ' + 'h: ' + height + 'px' + '<br />' +
                    'left: ' + positionLeft + 'px; ' + 'top: ' + positionTop + 'px' ;

                $(this).uitooltip( "option", "content", content );
                $(this).uitooltip("open");
            }).mouseenter(function () {
                $(this).uitooltip("disable");
            });

        });
    };

    var addBox = function () {
        var $box = jQuery('<div>', {class: 'box'}).css({position: 'absolute'});
        $box.interactive();

        $box.appendTo($board);
        $board.selectable('refresh');
    };

    var removeBox = function () {
        $board.find('.box.ui-selected').remove();
    };

    var saveBoard = function (e) {
        e.preventDefault();

        var boxes = [];

        // Find al boxes and images on the board
        $board.find('.box').each(function () {
            var $box = $(this);

            if ($box.find('img').length) {
                var image =  {
                    src: $box.find('img').attr('src'),
                    width: $box.find('img').attr('data-width'),
                    height: $box.find('img').attr('data-height')
                }
            }

            var box = {
                width: $box.width(),
                height: $box.height(),
                positionTop: $box.position().top,
                positionLeft: $box.position().left,
                image: image
            };

            boxes.push(box);
        });

        var data = {
            id: $boardIdInput.val(),
            boardName: $boardNameInput.val(),
            boxes: boxes
        };

        return $.ajax({
            url: $form.attr('action'),
            data: data,
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                $form.find('button').attr('disabled', 'disabled')
            },
            complete: function () {
                $form.find('button').removeAttr('disabled')
            },
            success:function (res) {
                $('.workarea').find('.alert').addClass('hide');
                $form.find('.text-danger').remove();

                if (res.success) {
                    $('.workarea').find('.alert-success').removeClass('hide');
                    $boardIdInput.val(res.boardId);

                    var $option = $boardSelect.find('option[value="' + res.boardId + '"]');

                    if ($option.length) {
                        $option.text(res.boardName)
                    } else {
                         $option = jQuery('<option>', {
                            attr: {
                                value: res.boardId,
                                selected: 'selected'
                            },
                            text: res.boardName
                        });

                        $option.appendTo($boardSelect);
                        $boardSelect.selectmenu("refresh");
                    }
                } else {
                    $('.workarea').find('.alert-danger').removeClass('hide');

                    if (res.messages) {
                        for (var inputName in res.messages) {
                            var errors = res.messages[inputName];

                            for (var key in errors) {
                                var error = errors[key];

                                var $error = jQuery('<p>', {
                                    class: 'help-block text-danger',
                                    text: error
                                });

                                $error.appendTo($form)
                            }
                        }
                    }
                }
            }
        });
    };

    var loadBoard = function () {
        $('.workarea').find('.alert').addClass('hide');
        $form.find('.text-danger').remove();
        $('.js-box-remove').attr('disabled', 'disabled');

        zoomBoard(1);

        $zoomControl.slider("value", 100);

        var boardId = $boardSelect.val();
        $board.html('');

        if (!boardId) {
            $boardNameInput.val('');
            $boardIdInput.val('');
            return;
        }

        $.ajax({
            url: '/load-board',
            type: 'post',
            dataType: 'json',
            data: {id: boardId},
            success: function (res) {

                if (res.boxes) {
                    for (var key in res.boxes) {
                        var box = res.boxes[key];

                        var $box = jQuery('<div>', {
                            class: 'box'
                        })
                            .css({width: box.width, height: box.height, top: box.positionTop, left: box.positionLeft, position: 'absolute'});

                        $box.interactive();

                        if (box.image) {
                            var imageWidth = box.image.width;
                            var imageHeight = box.image.height;
                            var maxWidth = $box.width();
                            var maxHeight = $box.height();

                            var size = calculateAspectRatioFit(imageWidth, imageHeight, maxWidth, maxHeight);

                            var $img = jQuery('<img>', {
                                src: box.image.src,
                                css: {
                                    width: size.width + 'px',
                                    height: size.height + 'px'
                                },
                                attr: {
                                    'data-width': imageWidth,
                                    'data-height': imageHeight
                                }
                            });

                            $img.appendTo($box);
                        }

                        $box.appendTo($board);
                    }
                }

                $boardNameInput.val(res.boardName);
                $boardIdInput.val(res.boardId);
            }
        })
    };

    var bindUIActions = function () {
        $('form').on('submit', saveBoard);

        $('.js-box-add').on('click', addBox);

        $board.selectable({
            selected: function (e, ui) {
                $('.js-box-remove').removeAttr('disabled');
            },
            unselected: function(e, ui) {
                $('.js-box-remove').attr('disabled', 'disabled');
            }
        });

        // See http://celery94.github.io/javascript/2016/01/21/jquery-ui-selectable-draggable.html
        $('body').on('click', '.box', function(e){
            e.stopPropagation();
            if (e.metaKey == false) {
                // if command key is pressed don't deselect existing elements
                $board.find('.box').removeClass("ui-selected");
                $(this).addClass("ui-selecting");
            } else {
                if ($(this).hasClass("ui-selected")) {
                    // remove selected class from element if already selected
                    $(this).removeClass("ui-selected");
                } else {
                    // add selecting class if not
                    $(this).addClass("ui-selecting");
                }
            }

            $board.data("uiSelectable")._mouseStop(null);
        });

        $('.js-box-remove').on('click', removeBox);

        $boardSelect.on('change', loadBoard);

        $imageContainer.find('img').draggable({
            containment: '.wrapper',
            helper: "clone",
            cursor: "move",
            revert: "invalid"
        });

        $zoomControl.slider({
            min: 0,
            max: 200,
            value: 100,
            slide: function (e, ui) {
                var zoomValue = ui.value / 100;

                zoomBoard(zoomValue);
            }
        });

        $boardSelect.selectmenu({
            position: {my: 'center bottom' , at: 'center top-10'},
            collision: "flip",
            change: function(event, ui) {
                $boardSelect.trigger('change')
            }
        });
    };

    var zoomBoard = function (zoomValue) {
        zoom = zoomValue;

        var css = {
            webkitTransform: 'scale(' + zoomValue +')',
            MozTransform: 'scale(' + zoomValue +')',
            msTransform: 'scale(' + zoomValue +')',
            OTransform: 'scale(' + zoomValue +')',
            transform: 'scale(' + zoomValue +')',
            width: $board.parent().width(),
            height: $board.parent().height()
        };

        $board.css(css);

        $('.zoom-value').text(zoomValue + 'x');
    };

    var init = function () {
        $.widget.bridge('uitooltip', $.ui.tooltip);

        bindUIActions();
    };

    return {
        init: init
    }
})();

$(function () {
    Application.init();
});
