<?php
namespace Application\Factory;

use Application\Controller\IndexController;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

class IndexControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $cpm)
    {
        $serviceLocator = $cpm->getServiceLocator();
        $dm = $serviceLocator->get('doctrine.documentmanager.odm_default');
        $formManager = $serviceLocator->get('FormElementManager');

        $controller = new IndexController($dm);
        $controller->setFormManager($formManager);

        return $controller;
    }
}
