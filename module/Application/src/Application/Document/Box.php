<?php
namespace Application\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument
 */
class Box
{
    /**
     * @ODM\Field(type="int")
     */
    protected $width;

    /**
     * @ODM\Field(type="int")
     */
    protected $height;

    /**
     * @ODM\Field(type="int")
     */
    protected $positionTop;

    /**
     * @ODM\Field(type="int")
     */
    protected $positionLeft;

    /**
     * @ODM\EmbedOne(targetDocument="Image")
     */
    protected $image;

    /**
     * @return mixed
     */
    public function getPositionLeft()
    {
        return $this->positionLeft;
    }

    /**
     * @param mixed $positionLeft
     */
    public function setPositionLeft($positionLeft)
    {
        $this->positionLeft = $positionLeft;
    }

    /**
     * @return mixed
     */
    public function getPositionTop()
    {
        return $this->positionTop;
    }

    /**
     * @param mixed $positionTop
     */
    public function setPositionTop($positionTop)
    {
        $this->positionTop = $positionTop;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage(Image $image)
    {
        $this->image = $image;
    }

    public function populate($data = [])
    {
        $this->width = $data['width'];
        $this->height = $data['height'];
        $this->positionLeft = $data['positionLeft'];
        $this->positionTop = $data['positionTop'];

        if ($data['image']) {
            $image = new Image();
            $image->populate($data['image']);
            $this->image = $image;
        }
    }

    public function getArrayCopy()
    {
        $vars = get_object_vars($this);
        $vars['image'] = $this->image ? $this->image->getArrayCopy() : null;

        return $vars;
    }
}