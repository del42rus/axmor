<?php
namespace Application\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument
 */
class Image
{
    /**
     * @ODM\Field(type="string")
     */
    protected $src;

    /**
     * @ODM\Field(type="int")
     */
    protected $width;

    /**
     * @ODM\Field(type="int")
     */
    protected $height;

    /**
     * @return mixed
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * @param mixed $src
     */
    public function setSrc($src)
    {
        $this->src = $src;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function populate($data = [])
    {
        $this->src = $data['src'];

        $this->height = $data['height'];
        $this->width = $data['width'];
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}